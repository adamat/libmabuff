/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _MAB_INPUT_H_
#define _MAB_INPUT_H_ 0

#include <string>
#include "zone.hpp"

namespace mabuff{
  unsigned getch(); // Waits until a character key is pressed
  unsigned getch(double timeout); // As getch(), but wait max. for 'timeout' seconds

  unsigned getkey(); // Waits until a nonstandard key is pressed
  unsigned getkey(double timeout); // As getkey(), but wait max. for 'timeout' seconds

  std::string getstr(Zone * field, unsigned maxlen = 0, unsigned bgch = ' ', unsigned delim = key_enter);
  // ^ Simple input shown in a zone; no navigation; w/ cursor, deleting allowed

  //bool handlestr(std::vector<unsigned>* str, double timeout, unsigned delim = '\n'); //Invisibly handles a string for an amount of time
  //bool handlestr(std::vector<unsigned>* str, Zone *field, double timeout, unsigned delim = '\n'); //Visibly handles a string for an amount of time
  // ^ These are for use in loops, so you can, for example, animate while waiting for a string
  // Return 'true', if the user continues to write, 'false' when the string is delim-ed
}

#endif
