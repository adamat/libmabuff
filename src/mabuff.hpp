/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _MAB_MABUFF_H_
#define _MAB_MABUFF_H_ 0

#ifndef MABUFF_VERSION
#define MABUFF_VERSION 1.2.1
#define MABUFF_VERSION_MAJOR 1
#define MABUFF_VERSION_MINOR 2
#define MABUFF_VERSION_REVISION 1
#endif

#include "zone.hpp"
#include "input.hpp"
#include <exception>
#include <chrono>
#include <ctime>
#include <cerrno>

namespace mabuff{

  void Init(unsigned width, unsigned height) throw(std::exception);
  void Terminate();
  void Print();
  void Wait(unsigned milliseconds);
  template <class Rep, class Period>
    void Wait(std::chrono::duration<Rep, Period> dur)
    {
      timespec ti;
      ti.tv_sec = std::chrono::duration_cast<std::chrono::seconds>(dur).count();
      ti.tv_nsec = (std::chrono::duration_cast<std::chrono::nanoseconds>(dur).count()) % 1000000000;

    while ((nanosleep(&ti, &ti) == -1) && (errno == EINTR))
      ;
    }

  // Changes the flags of the cells out of the initialised size
  void change_default_flags(unsigned fg_flags, unsigned bg_flags);
  void change_default_char(unsigned ch = ' ');

  extern Zone* Root;

  // Some common border settings:
  extern BorderSetting * simple; //Simple, one-line border (default)
  extern BorderSetting * doubled; //Two-line border
  extern BorderSetting * ascii; //Border made of +-|
}

#endif
