/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include <fstream>
#include "mabuff.hpp"
#include <cstdlib>
#include <vector>
#include <iostream>
#include "termbox.hpp"
#include <exception>

using namespace mabuff;
using namespace std;

Zone* inner_zone;

int main(){
  try{
    Init(20, 10);
  }catch (std::exception & e){
    std::cerr << "Couldn't initialise mabuffer: " << e.what() << std::endl;
    return 1;
  }

  Root->change_bg(white);
  Root->change_fg(black);
  Root->set_border(doubled, green);
  /*
  inner_zone = new Zone(16, 6, 1);
  inner_zone->move(2, 2);

  Zone limit_mask(4, 1, 2); //Highlights last four characters
  limit_mask.move(16, 0);
  limit_mask.fill(adopt);
  limit_mask.change_fg(red | bold);

  inner_zone->change_bg(blue);
  inner_zone->fill();
  inner_zone->set_border(simple, red, yellow);
  inner_zone->write("Use P to end", 0, 1, center, white | bold);
  Print();

  std::string str = getstr(Root, 19, '-', 'p');
  inner_zone->insert(str.begin(), str.end());
  */
  std::vector<unsigned> vec {
    'V', 'e', 'k', 't', 'o', 'r', 'o', 'v', L'ý', ' ', 't', 'e', 'x', 't'
  };
  Root->write("Toto je\n\tstringový textíček☺\n\frab\byc\fRdefghijklmnopqrstuvwxyz1234567890", 0, 2, mabuff::center, blue | bold);
  Print();
  getkey();
  Terminate();
  return 0;
}
