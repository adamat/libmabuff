/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "mabuff.hpp"
#include <set>
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace mabuff;
using namespace std;

void codegen();
void scroll_up();
void change_column();
void prepare_ui();
void select();
void colorify(unsigned c);
bool check_answer();
void push_view(bool down = 0);
string str_hits();
string str_colour_only();
void win();
void lose();
bool really_quit();
void help_me();

Zone in[4] = {Zone(3,2,2),Zone(3,2,2),Zone(3,2,2),Zone(3,2,2)};
unsigned sel[4] = {0,0,0,0};
unsigned code[4];

Zone ui(42,24,0);
Zone help(42,8,1);
Zone quit(42,3,2);

set<Zone*> list[2]; //columns
signed char selected = 0; //currently selecting slot
bool column = 0;
char passes = 0; //number of attempts

Zone mask_selected(3,2,8);


int main(){
  Init(42,24);
  codegen();
  bool done = 0;
  prepare_ui();
  unsigned ch = 0;
  while(!done){
    //Step ONE - get key
    while(selected < 4){
      select();
      ch = getkey();
      switch(ch){
        case key_ctrl_q:
        case 'Q': ch = 'q';
        case 'q':
          if(really_quit()) done = 1;
          else{ selected--; ch = 'a';}
          break;

        case 'Y':
        case 'y': if(sel[0] != yellow && sel[1] != yellow && sel[2] != yellow && sel[3] != yellow){
          colorify(yellow); sel[selected] = yellow; break;} selected--; break;
        case 'G':
        case 'g': if(sel[0] != green && sel[1] != green && sel[2] != green && sel[3] != green){
          colorify(green); sel[selected] = green; break;} selected--; break;
        case 'C':
        case 'c': if(sel[0] != cyan && sel[1] != cyan && sel[2] != cyan && sel[3] != cyan){
          colorify(cyan); sel[selected] = cyan; break;} selected--; break;
        case 'B':
        case 'b': if(sel[0] != blue && sel[1] != blue && sel[2] != blue && sel[3] != blue){
          colorify(blue); sel[selected] = blue; break;} selected--; break;
        case 'M':
        case 'm': if(sel[0] != magenta && sel[1] != magenta && sel[2] != magenta && sel[3] != magenta){
          colorify(magenta); sel[selected] = magenta; break;} selected--; break;
        case 'R':
        case 'r': if(sel[0] != red && sel[1] != red && sel[2] != red && sel[3] != red){
          colorify(red); sel[selected] = red; break;} selected--; break;
        case 'W':
        case 'w': if(sel[0] != white && sel[1] != white && sel[2] != white && sel[3] != white){
          colorify(white); sel[selected] = white; break;} selected--; break;

        case key_backspace:
        case key_backspace2:
        case 'x':
        case 'X': if(selected){sel[--selected] = 0; colorify(black);} selected--; break;

        case 'H':
        case 'h':
        case '?': help_me();
        default: selected--;
      }
      if(ch == 'q') break;
      ++selected;
    }
    if(done) break;
    selected = 0;
    scroll_up();
    if(check_answer()){done = true; win(); break;}
    sel[0] = 0; sel[1] = 0; sel[2] = 0; sel[3] = 0;
    passes++;
    if(passes == 5) change_column();
    else if(passes == 10){ done = true; lose();}
  }
  Terminate();
  return 0;
}

void codegen(){
  srand(time(NULL));
  for(int i = 0; i < 4; i++){
    switch(rand()%7){
      case 0: if(code[0] != yellow && code[1] != yellow && code[2] != yellow && code[3] != yellow){code[i] = yellow; break;};
      case 1: if(code[0] != green && code[1] != green && code[2] != green && code[3] != green){code[i] = green; break;};
      case 2: if(code[0] != cyan && code[1] != cyan && code[2] != cyan && code[3] != cyan){code[i] = cyan; break;};
      case 3: if(code[0] != blue && code[1] != blue && code[2] != blue && code[3] != blue){code[i] = blue; break;};
      case 4: if(code[0] != magenta && code[1] != magenta && code[2] != magenta && code[3] != magenta){code[i] = magenta; break;};
      case 5: if(code[0] != red && code[1] != red && code[2] != red && code[3] != red){code[i] = red; break;};
      case 7: if(code[0] != white && code[1] != white && code[2] != white && code[3] != white){code[i] = white; break;};
      default:i--;
    }
  }
}

void prepare_ui(){
  ui.move(0,0);
  Root->fill(' ');
  Root->change_bg(black);
  Root->change_fg(white);

  quit.fill(' ');
  quit.change_bg(magenta);
  quit.change_fg(white);
  quit.write("Do you really wish to quit?\n[Y][<┘] Yes\n[N][Esc] No way!",0,0,0);
  quit.move(0,24);

  help.fill(' ');
  help.change_bg(magenta);
  help.change_fg(white);
  help.write("Adam Matoušek's Logik v. 1.0\nx - Exact hit\no - Wrong position\n[Y][G][C][B][M][R][W] Select colour\n"\
             "[H][?] Hide/show this help\n[X][^H] Remove last colour\n[Q][^Q] Quit (will ask before exit)",0,1,0);
  help.write("▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁",0,0,0,magenta, adopt);
  help.write("[?]",0,0,mabuff::right,white|bold, magenta);
  help.move(0,23);

  ui.write("\vY\v\n    \n    ", 2, 20, mabuff::left, black, yellow);
  ui.write("\vG\v\n    \n    ", 6, 20, mabuff::left, black, green);
  ui.write("\vC\v\n    \n    ", 10, 20, mabuff::left, black, cyan);
  ui.write("\vB\v\n    \n    ", 14, 20, mabuff::left, white, blue);
  ui.write("\vM\v\n    \n    ", 18, 20, mabuff::left, white, magenta);
  ui.write("\vR\v\n    \n    ", 22, 20, mabuff::left, white, red);
  ui.write("\vW\v\n    \n    ", 26, 20, mabuff::left, black, white);
  ui.write("\vX\v\n╔═╗\n╚═╝", 31, 20, mabuff::left, white, blue);
  ui.write("^H", 32, 22, mabuff::left, yellow|bold);
  ui.write("\vQ\v\nBYE\nBYE", 35, 20, mabuff::left, yellow|bold, red);

  mask_selected.insert("\v↓\v\v↑\v", green|bold, adopt);

  for(int i=0; i<4; i++){
    in[i].change_fg(white);
    in[i].insert("╔═╗╚═╝");
  }
  in[0].move(2,17);
  in[1].move(6,17);
  in[2].move(10,17);
  in[3].move(14,17);
  Print();
}

void select(){
  mask_selected.set_visibility(true);
  mask_selected.move((int(selected)*4)+2, 17);
  Print();
}

void colorify(unsigned c){
  in[selected].change_bg(c);
  Print();
}

void scroll_up(){
  mask_selected.set_visibility(false);
  system("sleep 0.13s");
  set<Zone*>::iterator i;
  for(int u = 0; u < 4; u++){
    i = list[column].insert(new Zone(3,2,3)).first;
    (*i)->move(2+(column*20)+(u*4),16);
    (*i)->fill("┌─┐└─┘");
    (*i)->change_bg(sel[u]);
    (*i)->change_fg(white|bold);
    in[u].change_bg(black);
  }
  i = list[column].insert(new Zone(2,2,3)).first;
  (*i)->move(18+(column*20),16);
  string s = str_hits();
  s += str_colour_only();
  (*i)->write(s.c_str(), 0, 0, 0, white|bold);

  Print();
  for(int u = 0; u < 3; u++){
    system("sleep 0.13s");
    for(set<Zone*>::iterator i = list[column].begin(); i != list[column].end(); i++)
      (*i)->move_by(0,-1);
    Print();
  }
}

void push_view(bool down){
  for(set<Zone*>::iterator i = list[column].begin(); i != list[column].end(); i++)
    (*i)->move_by(0,down*2-1);
  for(set<Zone*>::iterator i = list[!column].begin(); i != list[!column].end(); i++)
    (*i)->move_by(0,down*2-1);
  for(int i = 0; i < 4; i++) in[i].move_by(0,down*2-1);
  ui.move_by(0,down*2-1);
  mask_selected.move_by(0,down*2-1);
}

void change_column(){
  for(int u = 0; u < 20; u++){
    for(int i = 0; i < 4; i++)
      in[i].move_by(1,0);
      mask_selected.move_by(1,0);
    system("sleep 0.06s");
    Print();
  }
  column = true;
}

bool check_answer(){
  if(code[0] == sel[0] && code[1] == sel[1] && code[2] == sel[2] && code[3] == sel[3]) return 1;
  else return 0;
}

string str_hits(){
  string s = "";
  for(int i=0; i<4; i++) if(code[i] == sel[i]) s += "x";
  return s;
}

string str_colour_only(){
  string s = "";
  for(int i=0; i<4; i++) if(code[i] != sel[i] && (sel[i] == code[0] || sel[i] == code[1] ||sel[i] == code[2] ||sel[i] == code[3])) s += "o";
  return s;
}

void win(){
  Root->fill(" WIN !");
  Root->change_fg(green);
  for(int t = 0; t < 50; t++){
    for(set<Zone*>::iterator i = list[column].begin(); i != list[column].end(); i++)
      (*i)->move_by(0,-(rand()%2+1));
    for(set<Zone*>::iterator i = list[!column].begin(); i != list[!column].end(); i++)
      (*i)->move_by(0,-(rand()%2+1));
    Print();
    system("sleep 0.08s");
  }
}

void lose(){
  Root->fill(" LOST !");
  Root->change_fg(red);
  for(int i = 0; i < 4; i++) in[i].change_fg(code[i]);
  for(int t = 0; t < 50; t++){
    for(set<Zone*>::iterator i = list[column].begin(); i != list[column].end(); i++)
      (*i)->move_by(0,rand()%2+1);
    for(set<Zone*>::iterator i = list[!column].begin(); i != list[!column].end(); i++)
      (*i)->move_by(0,rand()%2+1);
    Print();
    system("sleep 0.08s");
  }
}

bool really_quit(){
  for(int i = 0; i < 3; i++){
    push_view();
    quit.move_by(0, -1);
    Print();
    system("sleep 0.08s");
  }
  bool really, done = 0;
  while (!done){
    switch(getkey()){
      case 'y':
      case 'Y':
      case key_enter: done = true; really = true; break;

      case 'n':
      case 'N':
      case key_esc: done = true; really = false; break;

      default: break;
    }
  }
  if(!really){
    for(int i = 0; i < 3; i++){
      push_view(true);
      quit.move_by(0, 1);
      Print();
      system("sleep 0.08s");
    }
  }
  return really;
}

void help_me(){
  for(int i = 0; i < 7; i++){
    push_view();
    help.move_by(0, -1);
    Print();
    system("sleep 0.08s");
  }
  bool done = 0;
  while(!done){
    switch(getkey()){
      case 'H':
      case 'h':
      case key_esc:
      case '?': done = true; break;

      default: break;
    }
  }
  for(int i = 0; i < 7; i++){
    push_view(true);
    help.move_by(0, 1);
    Print();
    system("sleep 0.08s");
  }
}
