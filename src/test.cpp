/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "mabuff.hpp"
#include <cstdlib>
#include <exception>
#include <iostream>
#include <chrono>
using namespace mabuff;

int main(){
  try{
    Init(40, 15);
  }catch (std::exception & e){
    std::cerr << "Couldn't initialise mabuffer: " << e.what() << std::endl;
    return 1;
  }

  Root->change_bg(blue);
  Root->change_fg(yellow);
  Root->fill(".´");
  Root->set_border(yellow|bold);
  Print();
  Wait(1000);
  change_default_flags(red, keep);
  change_default_char('*');
  Zone tx(20,15, -1);
  //tx.change_bg(red);
  tx.insert("We\vhave\vcookies\vand\vtransparent\vcells", adopt|blue|bold, adopt|keep);
  Print();
  Wait(std::chrono::milliseconds(1000));
  change_default_flags(terminal | bold, green);
  change_default_char('a');
  Root->write("Left alignment,\npreviously known as keepcol", 2, 3, left, yellow|bold, keep);
  Print();
  Wait(std::chrono::microseconds(1500000));
  change_default_flags(yellow | bold, yellow);
  change_default_char('.');
  Root->write("Right alignment can be\nmulti-line,\nvery, very multiline.\nThe first line *will* work!", 2, 5, right, white|bold);
  Print();
  Wait(std::chrono::seconds(1));
  Root->write("Center Alignment\nRulezz!\n(we need only\nComic Sans now)", 0, 9, center, cyan|bold);
  Root->write("   THE ORIGINAL   ", 0, 2, center, terminal, terminal);
  Print();

  Wait(5000);
  Terminate();
  return 0;
}
