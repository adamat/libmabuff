/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "input.hpp"
#include "mabuff.hpp"
#include "termbox.hpp"
#include <vector>
#include <iostream>

namespace mabuff{
  unsigned getch(){
    tb::event e;
    while(tb::poll_event(e) && e.ch == 0);
    return e.ch;
  }

  unsigned getch(double timeout){
    tb::event e;
    tb::peek_event(e, timeout*1000);
    return e.ch;
  }

  unsigned getkey(){
    tb::event e;
    while(tb::poll_event(e) && e.key == 0 && e.ch == 0);
    return (e.key)?(e.key):(e.ch);
  }

  unsigned getkey(double timeout){
    tb::event e;
    while(tb::peek_event(e, timeout*1000) && e.key == 0 && e.ch == 0);
    return (e.key)?(e.key):(e.ch);
  }

  std::string getstr(Zone * field, unsigned maxlen, unsigned bgch, unsigned delim){
    tb::event e;
    std::vector<unsigned> buf;
    //initial cursor position
    tb::set_cursor(field->get_x(), field->get_y());
    Print();
    do{
      while(tb::poll_event(e) && e.key == 0 && e.ch == 0);
      if((e.key == key_backspace || e.key == key_backspace2) && !buf.empty())
        buf.pop_back(); //Backspace deletes
      else if(e.ch == delim || e.key == delim) break;
      else if(e.ch && (buf.size() < maxlen || !maxlen))
        buf.push_back(e.ch);
      else{
        std::cout << '\a'; //beep!
        Print(); //actually, NOW beep!
        continue;
      }

      // The "graphical" part
      field->fill(bgch);
      //how many characters (from the start) are overlapping
      if(!buf.empty()){
        int hidden = buf.size() - field->get_width();
        if(hidden < 0) hidden = 0; // the field is long enough
        else hidden++; //make space for cursor
        field->insert(buf.begin() + hidden, buf.end());
      }
      //placing the cursor
      unsigned short cursoff = buf.size(); //cursor offset
      if(cursoff >= field->get_width()) cursoff = field->get_width() - 1;
      tb::set_cursor(field->get_x() + cursoff, field->get_y());

      Print();
    } while(true); // is broken manually

    tb::set_cursor(-1, -1); //hide cursor
    //string creation
    std::string str;
    for(std::vector<unsigned>::iterator it = buf.begin(); it != buf.end(); it++){
      char ch[6];
      int l = tb::utf8_unicode_to_char(ch, *it); //returns the # of bytes
      str.append(ch, l);
    }
    return str;
  }
}
