/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "view.hpp"
#include "snake.hpp"
#include <mabuff/mabuff.hpp>
#include <mabuff/log.hpp>
#include <sstream>
using namespace mabuff;

Zone *z_field = 0;
Zone *z_snake = 0;
Zone *z_pause = 0;
Zone *z_quit = 0;
Zone *z_points = 0;

void draw_atom(unsigned pos){
  unsigned y = pos / z_snake->get_width();
  unsigned x = pos % z_snake->get_width();
  z_snake->write("#", x, y, 0, green, black);
}

void draw_invalid(unsigned pos){
  unsigned y = pos / z_field->get_width();
  unsigned x = pos % z_field->get_width();
  z_field->write(" ", x, y, 0, 0, yellow);
}

void draw_food(unsigned pos){
  unsigned y = pos / z_field->get_width();
  unsigned x = pos % z_field->get_width();
  z_field->write("*", x, y, 0, yellow | bold);
}

void draw_points(unsigned pts){
  std::ostringstream os;
  os << pts;
  z_points->write(os.str().c_str(), 0, 0, right);
}

void draw_pause(bool visible){
  z_pause->set_visibility(visible);
  draw();
}

void draw_quit(){
  z_quit->set_visibility(true);
  draw();
  while(getkey() != 'q');
}

#include <iostream>
void draw_initialise(){
  std::cerr<<"init\n";
  mabuff::Verbosity = mabuff::INFO;
  Init(42, 24);
  std::cerr<<"Root setting\n";
  Root->change_bg(white);
  Root->change_fg(blue);
  Root->fill();
  Root->set_border();
  std::cerr<<"Root setting done\n";

  z_field = new Zone(40, 20, 1);
  z_field->move(1, 3);
  z_field->change_bg(black);
  z_field->change_fg(white);
  z_field->fill();

  z_snake = new Zone(40, 20, 2);
  z_snake->move(1, 3);
  z_snake->fill('\v');

  z_pause = new Zone(40, 20, 3);
  z_pause->move(1, 3);
  z_pause->change_bg(blue);
  z_pause->change_fg(white);
  z_pause->fill('.');
  z_pause->write("PAUSED", 0, 9, center, yellow | bold);
  z_pause->set_visibility(false);

  z_quit = new Zone(40, 20, 3);
  z_quit->move(1, 3);
  z_quit->change_bg(red);
  z_quit->change_fg(white);
  z_quit->fill('.');
  z_quit->write("GAME OVER", 0, 6, center, yellow | bold);
  z_quit->write("Press [q] to quit", 0, 11, center, yellow | bold);
  z_quit->set_visibility(false);

  z_points = new Zone(10, 1, 2);
  z_points->move(31, 1);
  z_points->change_fg(red | bold);
  z_points->fill();

  Root->write("Score", 1, 2, right, black | bold);
}

void draw_teminate(){
  Terminate();
  delete z_field;
  delete z_snake;
}

void draw(){
  z_snake->fill('\v');
  z_field->fill(' ');
  snake->draw();
  field->draw();
  Print();
}
