-- PLEASE IGNORE THIS FILE NOW --

MABuffer Color Mask
===================

A mask file is a plain stream of bytes (without any formating).
Each byte has the following format:
  0 0 0  0 0 0 0 0
  ^ ^ ^  ^ ^ ^ ^ ^
  | | |  | | | | `- Red foreground
  | | |  | | | `--- Green foreground
  | | |  | | `----- Blue foreground
  | | |  | `------- Adopt flag
  | | |  `--------- Bold
  | | | 
  | | `------------ Red background
  | `-------------- Green background
  `---------------- Blue background

Create those files using hex editor or using a special utility, mcmconv, once
it is included with MABuffer.
